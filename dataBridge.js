const udp = require('dgram');
const server = udp.createSocket('udp4');
let handleData = () => {};

const SET_COLOR = 0;
const SET_BRIGHTNESS = 1;
const SET_SMOOTHINESS = 2;
const FADE_TO = 3;
const STROB = 4;
const OFF = 9;

let address, port;

const sendData = (data) => {
  if (address && port) {
    server.send(Buffer.from(data), 0, data.length, port, address);
  }
};

const setIncomingDataHandler = (fn) => {
  handleData = fn;
};

const executeCommand = (command, ...args) => {
  try {
    sendData([command, ...args].map(it => parseInt(it)));
  } catch (e) {}
}

const commands = {
  setColor: (r, g, b) => executeCommand(SET_COLOR, r, g, b),
  setBrightness: (brightness) => executeCommand(SET_BRIGHTNESS, brightness),
  setSmoothiness: (smoothiness) => executeCommand(SET_SMOOTHINESS, smoothiness),
  fadeTo: (r, g, b, brightness) => executeCommand(FADE_TO, r, g, b, brightness),
  strob: (freq) => executeCommand(STROB, freq),
  off: () => executeCommand(OFF),
}

server.on('message', (msg, info) => {
  address = info.address;
  port = info.port;

  handleData(new DataView(msg.buffer));
});

server.bind(2060);

module.exports = {
  commands,
  sendData,
  setIncomingDataHandler,
};
