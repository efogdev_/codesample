const {RGBAudioController} = require('./controller.js');
const RGB = require('./RGB');
const { commands } = require('./dataBridge.js');

const AudioController = new RGBAudioController();
const express = require('express');
const cors = require('cors');
const server = express();

server.use(cors());
server.use(express.static(__dirname + '/web/build'));

server.get('/status', (req, res) => {
  res.status(200).send(AudioController.mode);
});

server.get('/color', (req, res) => {
  const { r, g, b } = req.query;

  AudioController.setMode('color');
  AudioController.mode.color = [parseInt(r), parseInt(g), parseInt(b)];

  commands.setColor(...AudioController.mode.color);
  res.sendStatus(200);
});

server.get('/brightness', (req, res) => {
  const { value } = req.query;

  AudioController.setMode('color');
  AudioController.mode.brightness = Math.max(0, Math.min(100, parseInt(value)));
  commands.setBrightness(AudioController.mode.brightness);
  res.sendStatus(200);
});

server.get('/music', (req, res) => {
  AudioController.setMode('club');
  res.sendStatus(200);
});

server.get('/strob', (req, res) => {
  const { freq } = req.query;

  AudioController.setMode('strob');
  AudioController.mode.freq = parseInt(freq);
  commands.strob(AudioController.mode.freq);
  res.sendStatus(200);
});

server.listen(4133, () => {
  console.log('Server started.');
});
