const {RGBAudioController, TRANSFORM_PERIOD} = require('./controller.js');
const RGB = require('./RGB');
const {
  QMainWindow,
  WidgetEventTypes,
  QPainter,
  QWidget,
  QColor,
  QKeyEvent,
  FlexLayout,
} = require("@nodegui/nodegui");
const {commands} = require('./dataBridge');

const FPS = 180;

const AudioController = new RGBAudioController();
AudioController.start();
const rgb = new RGB([]);

const mainWindow = new QMainWindow();
mainWindow.setWindowTitle('Visualizer');
mainWindow.setGeometry(0, 0, 1920, 360);

const beatAnalyzerWindow = new QMainWindow();
beatAnalyzerWindow.setWindowTitle('Beat analyzer');
beatAnalyzerWindow.setGeometry(0, 455, 1920, 600);

// const center = new QWidget();
// const layout = new FlexLayout();
// center.setLayout(layout);

beatAnalyzerWindow.show();
beatAnalyzerWindow.setInlineStyle('background-color: #000;')

mainWindow.show();
mainWindow.setInlineStyle('background-color: #000;')

let hold = false;
let showTriggersOnly = false;
let graphData = [];
let trigger = 0;
let MAX_dB = 0;

mainWindow.addEventListener('KeyPress', e => {
  const event = new QKeyEvent(e);
  const keymap = {
    83: 's',
    84: 't',
    72: 'h',
    79: 'o',
    49: '1',
    50: '2',
    51: '3',
    52: '4',
    53: '5',
  };

  switch (keymap[event.key()]) {
    case 's':
      commands.strob(10);
      break;
    case 't':
      showTriggersOnly = !showTriggersOnly;
      break;
    case 'h':
      hold = !hold;
      break;
    case 'o':
      commands.off();
      break;
    case '1':
      commands.setColor(255, 0, 0);
      break;
    case '2':
      commands.setColor(0, 255, 0);
      break;
    case '3':
      commands.setColor(0, 0, 255);
      break;
    case '4':
      commands.setColor(255, 255, 255);
      break;
    case '5':
      commands.setBrightness(100);
      break;
    default:
      break;
  }
})

let repaintTimer = setInterval(() => {
  try {
    mainWindow.repaint();
    beatAnalyzerWindow.repaint();
  } catch (e) {}
}, Math.floor(1000 / FPS));

mainWindow.addEventListener(WidgetEventTypes.Paint, () => {
  const painter = new QPainter(mainWindow);

  try {
    const geometry = mainWindow.geometry();
    const availableWidth = geometry.width() - 100;
    const SAMPLE_RATE = AudioController.getSampleRate();

    painter.setPen(new QColor(255, 255, 255))
    painter.setBrush(new QColor(255, 255, 255))

    painter.drawLine(0, geometry.height() - 1, availableWidth, geometry.height() - 1);
    painter.drawLine(0, 0, 0, geometry.height());

    painter.drawLine(availableWidth, 0, availableWidth, geometry.height());
    painter.drawText(10, 18, `${Math.floor(SAMPLE_RATE / 2 / (graphData.length - 1))} Hz`);
    painter.drawText(availableWidth - 70, 18, `${Math.floor(SAMPLE_RATE / 2)} Hz`);

    if (!graphData.length)
      throw new Error();

    const brightness = rgb.getBrightness(AudioController.mode.name, graphData, SAMPLE_RATE);

    painter.drawLine(geometry.width(), 0, geometry.width(), geometry.height());
    painter.setPen(new QColor(0, 200, 120));

    if (trigger) {
      for (let i = 0; i <= 99; i++) {
        painter.drawLine(geometry.width() - i, 0, geometry.width() - i, geometry.height());
      }
    }

    const pixelsForFrequency = availableWidth / (graphData.length - 1);
    for (let i = 0; i <= graphData.length - 2; i++) {
      const index = i + 1;
      const dB = graphData[index];
      const dB_percentage = dB / MAX_dB;

      const y = geometry.height() * dB_percentage;
      const normalizedY = geometry.height() - y;

      for (let j = 0; j <= pixelsForFrequency; j++) {
        const frequency = (i + 1) * SAMPLE_RATE / 2 / graphData.length;

        if (frequency < RGB.LOW_FREQUENCY_BOTTOM_THRESHOLD) {
          painter.setPen(new QColor(200, 0, 0, 255));
        } else if (i === 0 || frequency > RGB.LOW_FREQUENCY_BOTTOM_THRESHOLD && frequency < RGB.LOW_FREQUENCY_TOP_THRESHOLD) {
          painter.setPen(new QColor(0, 200, 0, 255));
        } else {
          painter.setPen(new QColor(0, 140, 255, 255));
        }

        painter.drawLine(i * pixelsForFrequency + j, geometry.height(), i * pixelsForFrequency + j, normalizedY);
      }
    }

    painter.end();
  } catch (e) {
    painter.end();
  }
});

const frequenciesToAnalyze = [100, 150, 200, 250, 300, 350, ];
const points = {};

beatAnalyzerWindow.addEventListener(WidgetEventTypes.Paint, () => {
  const painter = new QPainter(beatAnalyzerWindow);

  try {
    const areaHeight = 64;
    const margin = 18;
    const geometry = beatAnalyzerWindow.geometry();
    const availableWidth = geometry.width() - 100;

    painter.setPen(new QColor(255, 255, 255));

    const GAIN_MAX = RGB.GAIN_MAX;
    frequenciesToAnalyze.forEach((freq, index) => {
      const standardMargin = margin + index * (areaHeight + margin);

      painter.setPen(new QColor(255, 255, 255));
      painter.drawText(availableWidth - 50, standardMargin, `< ${freq} Hz`);

      if (!points[freq])
        points[freq] = [];

      if (points[freq].length > availableWidth)
        points[freq].splice(0, points[freq].length - availableWidth - 1);

      points[freq].forEach(({ peak, average }, i) => {
        const peakY = standardMargin + areaHeight / GAIN_MAX * peak;
        const averageY = standardMargin + areaHeight / GAIN_MAX * average;

        painter.setPen(new QColor(255, 0, 0));
        painter.drawLine(i, peakY, i, peakY)

        painter.setPen(new QColor(0, 255, 0));
        painter.drawLine(i, averageY, i, averageY)
      });
    });

    painter.end();
  } catch (e) {
    painter.end();
  }
});

handlePoints = (dataPoints) => {
  const SAMPLE_RATE = AudioController.getSampleRate();
  const FREQUENCY_STEP = SAMPLE_RATE / 2 / dataPoints.length;

  frequenciesToAnalyze.forEach((freq, index) => {
    if (!points[freq])
      points[freq] = [];

    let startIndex = 0, endIndex;

    if (index !== 0)
      startIndex = Math.floor(freq / FREQUENCY_STEP);
    endIndex = Math.ceil(freq / FREQUENCY_STEP) + 1;

    const data = dataPoints.slice(startIndex, endIndex).filter(it => it);

    points[freq].push({
      peak: Math.max(...data),
      average: data.reduce((sum, item) => sum + item, 0) / data.length,
    });
  });
}

AudioController.setTriggerFn(isTrigger => {
  if (hold)
    return;

  trigger = isTrigger;
});

AudioController.setDrawFn(f => {
  if (!Array.isArray(f))
    return;

  if (hold)
    return;

  if (showTriggersOnly && !trigger) {
    return;
  }

  try {
    graphData = f;
    MAX_dB = RGB.GAIN_MAX;//Math.max(...f);

    handlePoints(graphData);
  } catch (e) {}
});

global.mainWindow = mainWindow;
global.beatAnalyzerWindow = beatAnalyzerWindow;
