const ft = require('fourier-transform');
const RGB = require('./RGB');
const dataBridge = require('./dataBridge');
const { setIncomingDataHandler } = dataBridge;
const { fadeTo, off, setBrightness, setColor, setSmoothiness } = dataBridge.commands;

const BUFFER_LENGTH = 1024 * 128;
const TRANSFORM_PERIOD = 48; // MS
const TRANSFORM_COUNT = 1024;

const TRANSFORM_MODE = 1; // 1 - count, 0 - period

const IDLE_SMOOTHINESS = 150; // MS / 10
const IDLE_THRESHOLD = IDLE_SMOOTHINESS * 10; // MS

class RGBAudioController {
  constructor() {
    this.pause = true;

    this.MODES = [
      {name: 'color', color: [255, 255, 180], brightness: 20},
      {name: 'strob', freq: 10},
      {name: 'club', smoothiness: 12},
    ];

    this.SAMPLE_RATE = 0;
    this.idleId = 0;
    this.lastIdleColor = [0, 0, 0];
    this.lastIndex = 0;
    this.lastResult = new Date().getTime();
    this.data = [];
    this.dataOffset = [];
    this.setMode('color');

    this.RGB = new RGB([]);

    this.color = {
      r: 255,
      g: 255,
      b: 0,
    }

    this.log('Listening.');
  }

  start() {
    this.pause = false;
    setIncomingDataHandler(this.acceptData.bind(this));
  }

  stop() {
    this.pause = true;
    this.stopIdle();
  }

  setMode(name) {
    this.mode = this.findMode(name);
    setSmoothiness(this.mode.smoothiness);

    if (this.mode.name !== 'club') {
      this.stop();
    } else {
      this.start();
    }
  }

  findMode(name) {
    return this.MODES.find(it => it.name === name);
  }

  getSampleRate() {
    return this.SAMPLE_RATE;
  }

  getTimestampString() {
    return `[${new Date().toLocaleString()}]`;
  }

  log(message) {
    console.log(`${this.getTimestampString()} ${message}`);
  }

  logTime(fn, label) {
    const _label = `${this.getTimestampString()} ${label}`;

    console.time(_label);
    fn();
    console.timeEnd(_label)
  }

  setTriggerFn(fn) {
    this.triggerFn = fn;
  }

  setDrawFn(drawFn) {
    this.drawFn = drawFn;
  }

  idle() {
    if (this.idleId || this.pause)
      return;

    this.idleId = setInterval(() => {
      if (this.pause)
        return;

      const color = this.RGB.generateRGB(this.lastIdleColor);

      setSmoothiness(IDLE_SMOOTHINESS);
      fadeTo(...color, 20);

      this.lastIdleColor = color;
    }, IDLE_SMOOTHINESS * 10);
  }

  stopIdle() {
    clearInterval(this.idleId);
    this.idleId = 0;
  }

  output(data) {
    return new Promise(resolve => {
      setTimeout(() => {
        try {
          const result = this.RGB.handle(this.mode, data, this.SAMPLE_RATE);

          if (this.triggerFn)
            this.triggerFn(false);

          if (result) {
            resolve(result);

            this.lastResult = new Date().getTime();
            this.stopIdle();

            setTimeout(() => {
              const tasks = Array.isArray(result) ? [...result] : [result];

              tasks.forEach(task => {
                dataBridge.commands[task.command](...task.args);
              });
            });
          } else {
            resolve(null);

            if (new Date().getTime() - this.lastResult > IDLE_THRESHOLD) {
              this.idle();
            }
          }
        } catch (e) {
          console.log(e);
        }
      });
    });
  }

  addData(data, time) {
    if (this.data.length > BUFFER_LENGTH)
      this.data = [];

    this.dataOffset++;
    this.data.push({data, timestamp: time || new Date().getTime()});
  }

  acceptData(data, logRate = false, logTransformSpeed = false, defaultTime) {
    if (this.pause)
      return;

    if (typeof data !== 'number') {
      const time = new Date().getTime();
      const length = data.byteLength;

      for (let i = 0; i < length / 2; i++) {
        this.acceptData(data.getUint16(i * 2, true), logRate, logTransformSpeed, time);
      }

      if (TRANSFORM_MODE === 0) {
        this.processData(logRate, logTransformSpeed);
      }

      return;
    }

    this.addData(data, defaultTime);

    if (TRANSFORM_MODE === 1) {
      if (this.dataOffset % 64 === 0) {
        this.dataOffset = 0;
        this.processData(logRate, logTransformSpeed);
      }
    }
  }

  processData(logRate, logTransformSpeed) {
    try {
      const item = this.data[this.data.length - 1];
      const currentTimestamp = new Date().getTime();
      let dataToInterpolate;
      let startIndex = 0;

      let datas = this.data;
      let early = this.data[0].timestamp;
      let late = item.timestamp;

      const msPassed = late - early;
      const datasReceived = datas.length;

      this.SAMPLE_RATE = Number(1000 / (msPassed / datasReceived)).toFixed();

      if (logRate && late - early > 1000 && ((this.data.length / 10).toFixed() % 3 === 0)) {
        this.log(`Sample rate: ${this.SAMPLE_RATE} Hz`);
      }

      if (TRANSFORM_MODE === 0) {
        for (let i = this.data.length - 1; i > this.lastIndex; i--) {
          if (!this.data[i])
            break;

          const item = this.data[i];
          const count = this.data.length - 1 - i;

          if (currentTimestamp - item.timestamp >= TRANSFORM_PERIOD && count !== 0 && count !== 1 && (count & (count - 1)) === 0) {
            startIndex = i;
            break;
          }
        }

        if (startIndex) {
          dataToInterpolate = this.data.slice(startIndex, this.data.length - 1).map(it => it.data);
        }
      } else {
        dataToInterpolate = this.data.slice(-TRANSFORM_COUNT).map(it => it.data);
      }

      if (dataToInterpolate) {
        const dotsCount = dataToInterpolate.length;

        if (dotsCount & (dotsCount - 1) !== 0 || dotsCount < 128)
          return;

        const doTransform = () => {
          try {
            const result = ft(dataToInterpolate).slice(1, -4);
            const trigger = this.output(result);

            trigger.then(triggerResult => {
              if (triggerResult && this.triggerFn) {
                this.triggerFn(true);
              } else if (this.triggerFn) {
                this.triggerFn(false);
              }

              this.drawFn && this.drawFn.bind(this)(result);
            });
          } catch (e) {}
        };

        if (logTransformSpeed) {
          this.logTime(() => {
            this.log(`Got ${dotsCount} dots.`);
            doTransform();
          }, `Transforming`);
        } else {
          doTransform();
        }

        this.lastIndex = this.data.length - 1;
      }
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = {
  RGBAudioController,
  TRANSFORM_PERIOD,
};
