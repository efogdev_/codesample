module.exports = class RGB {
  static LOW_FREQUENCY_BOTTOM_THRESHOLD = 40;
  static LOW_FREQUENCY_TOP_THRESHOLD = 150;

  static GAIN_MAX = 256;

  static DEFAULT_BRIGHTNESS_ON_FAULT = .2;
  static DEFAULT_SMOOTHINESS = 1000;
  static HISTORY_MS = 2500;
  static CHILD_HISTORY_MS = 100;

  constructor(history) {
    this.history = history;
    this.childHistory = history;
    this.lastTrigger = 0;
    this.lastColor = 0;

    this.handle = this.handle.bind(this);
  }

  updateHistory(data, time, updateChild = false) {
    if (updateChild) {
      const reverseChildIndex = this.childHistory.slice().reverse().findIndex(it => time - it.time > RGB.CHILD_HISTORY_MS);
      const deleteChildIndex = reverseChildIndex === -1 ? null : this.childHistory.length - reverseChildIndex;

      if (deleteChildIndex && this.childHistory.length - deleteChildIndex > 30)
        this.childHistory.splice(0, deleteChildIndex);

      this.childHistory.push({data, time});
    }

    const reverseIndex = this.history.slice().reverse().findIndex(it => time - it.time > RGB.HISTORY_MS);
    const deleteIndex = reverseIndex === -1 ? null : this.history.length - reverseIndex - 1;

    if (deleteIndex)
      this.history.splice(0, deleteIndex);

    this.history.push({data, time});
  }

  getMsPerBeat() {
    if (this.history.length < 5)
      return null;

    const diffs = this.history.map((item, index) => index > 0 && item.time - this.history[index - 1].time).filter(it => it);
    const medianDiff = diffs.sort()[Math.floor(diffs.length / 2)];

    const filtered = diffs.filter(it => it > medianDiff);
    return Math.round(filtered.reduce((sum, diff) => sum + diff, 0) / filtered.length);
  }

  generateRandomByte() {
    return Math.floor(Math.random() * 255);
  }

  generateZeroOr255() {
    return Math.random() > 0.5 ? 0 : 255;
  }

  generateRGB(notLike = null) {
    let randomRGB = [0, 0, 0];
    while (randomRGB.every(it => it === 0) || (notLike && notLike.every((it, index) => randomRGB[index] === it))) {
      randomRGB = [this.generateZeroOr255(), this.generateZeroOr255(), this.generateZeroOr255()];
    }

    return randomRGB;
  }

  getBrightness(mode, ...args) {
    try {
      return this[`${mode.name}GetBrightness`](...args);
    } catch (e) {
      return null;
    }
  }

  handle(mode, ...args) {
    try {
      return this[`${mode.name}Handle`].bind(this)(...args);
    } catch (e) {
      return null;
    }
  }

  getWeights(data, sampleRate) {
    try {
      const array = data.slice();

      const frequenciesCount = Math.floor(sampleRate / 2 / array.length);
      const frequencies = new Array(frequenciesCount).fill(null).map((_, index) => (index + 1) * frequenciesCount);

      return frequencies.map((freq, index) => ({freq, gain: array[index]})).filter(it => it.gain).sort((a, b) => b.gain - a.gain);
    } catch (e) {
      return [];
    }
  }

  clubGetBrightness(data, sampleRate, time) {
    const BRIGHTNESS_MINIMUM = .4;
    const weights = this.getWeights(data, sampleRate);

    const lowFrequenciesGain = weights.filter(it => it.freq >= RGB.LOW_FREQUENCY_BOTTOM_THRESHOLD && it.freq <= RGB.LOW_FREQUENCY_TOP_THRESHOLD).map(it => it.gain);
    const anotherFreqs = weights.filter(it => it.freq > RGB.LOW_FREQUENCY_TOP_THRESHOLD).map(it => it.gain);

    if (!lowFrequenciesGain.length)
      lowFrequenciesGain.push(data[0]);

    const lowFrequenciesMax = Math.max(...lowFrequenciesGain);
    const otherMax = Math.max(...anotherFreqs);

    this.updateHistory(lowFrequenciesMax, time, true);

    const history = this.childHistory.map(it => it.data);

    if (history[history.length - 1] > (RGB.GAIN_MAX / 2.2)) {
      const samplesCount = sampleRate / 3500; // magic
      let samples = history.slice();

      if (history.length > samplesCount)
        samples = history.slice(-samplesCount);

      if (samples[0] > samples[samples.length - 1])
        return BRIGHTNESS_MINIMUM;

      const analyze = samples.map((sample, index) => {
          return (index === samples.length ? 1 : sample < samples[index + 1]);
      });

      const flowPercent = analyze.filter(Boolean).length / samples.length;

      if (flowPercent > .27)
        return 1;
    }

    return BRIGHTNESS_MINIMUM;
  }

  clubHandle(data, sampleRate) {
    const TRIGGER_THRESHOLD_MS = 320;
    // const TRIGGER_THRESHOLD_MS = 0;
    const time = new Date().getTime();
    const brightness = this.clubGetBrightness(data, sampleRate, time);

    if (brightness === 1) {
      if (time - this.lastTrigger <= TRIGGER_THRESHOLD_MS)
        return null;

      const randomRGB = this.generateRGB(this.lastColor);

      this.updateHistory(data, time);
      this.lastColor = randomRGB.slice();
      this.lastTrigger = time;

      return [
        {command: 'setBrightness', args: [100]},
        {command: 'setColor', args: [...randomRGB]},
        {command: 'setSmoothiness', args: [Math.round((this.getMsPerBeat() || RGB.DEFAULT_SMOOTHINESS) / 10)]},
        {command: 'fadeTo', args: [...randomRGB, 60]},
      ];
    } else {
      return null;
    }
  }
}
