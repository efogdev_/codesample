import React from 'react';
import '@blueprintjs/core/lib/css/blueprint.css';
import {
  Alignment,
  Navbar,
  NavbarGroup,
  NavbarHeading,
  Tab,
  Tabs,
  Card,
  Slider,
  NumericInput,
  Spinner,
} from '@blueprintjs/core';

import iro from '@jaames/iro';
import styles from './app.module.scss';

class App extends React.PureComponent {
  constructor(props) {
    super(props);

    this.colorPicker = null;
    this.api = '';

    this.state = {
      loading: true,
      navbarTabId: 0,
      brightness: 20,
      freq: 10,
      color: '#ffffab',
    };
  }

  rgbToHex(r, g, b) {
    const componentToHex = c => {
      if (isNaN(c) || c < 0 || c > 255)
        return 'ff';

      const hex = c.toString(16);

      return hex.length === 1 ? '0' + hex : hex;
    }

    return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
  }

  handleNavbarTabChange(id) {
    this.setState({ navbarTabId: id }, () => {
      if (id === 0) {
        this.changeBrightness(this.state.brightness);
        this.changeColor(this.state.color);
      }

      if (id === 1) {
        this.apiCall('/music');
      }

      if (id === 2) {
        this.setStrob(this.state.freq);
      }
    });
  }

  apiCall(endpoint, data) {
    const dataString = !data ? '' : `?${Object.keys(data).map(key => `${key}=${data[key]}`).join('&')}`;

    return fetch(this.api + endpoint + dataString)
      .then(it => it.json());
  }

  changeColor(color) {
    this.setState({ color }, () => {
      this.apiCall('/color', {
        r: parseInt(color.substr(1, 2), 16),
        g: parseInt(color.substr(3, 2), 16),
        b: parseInt(color.substr(5, 2), 16),
      });
    });
  }

  changeBrightness(brightness) {
    this.setState({ brightness }, () => {
      this.apiCall('/brightness', {
        value: brightness,
      });
    });
  }

  setStrob(freq) {
    this.setState({ freq }, () => {
      this.apiCall('/strob', {
        freq,
      })
    });
  }

  componentDidMount() {
    this.apiCall('/status')
      .then(data => {
        if (!data.name)
          return;

        if (data.name === 'color') {
          this.setState({
            loading: false,
            navbarTabId: 0,
            color: this.rgbToHex(...data.color),
            brightness: data.brightness,
          });
        } else if (data.name === 'strob') {
          this.setState({
            loading: false,
            navbarTabId: 2,
            freq: data.freq,
          });
        } else {
          this.setState({
            loading: false,
            navbarTabId: 1,
          });
        }
      });
  }

  mountPicker() {
    if (this.colorPicker)
      return;

    this.colorPicker = new iro.ColorPicker('#color-picker', {
      width: 300,
      color: this.state.color,
    });

    this.colorPicker.on('color:change', color => {
      this.changeColor(color.hexString);
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <div className={styles.loadingContainer}>
          <Spinner />
        </div>
      );
    }

    return (
      <div className="App">
        <Navbar className={styles.navbar}>
          <NavbarGroup align={Alignment.LEFT}>
            <NavbarHeading>
              <img alt="logo" className={styles.logo} src={require('./images/bulb.png')}/>
              <span className={styles.title}>SmartLigth</span>
            </NavbarHeading>
          </NavbarGroup>
        </Navbar>

        <div className={styles.head}>
          <Tabs
            className={styles.tabs}
            animate={true}
            id="navbar"
            large={true}
            onChange={id => this.handleNavbarTabChange(id)}
            selectedTabId={this.state.navbarTabId}
          >
            <Tab id={0} title="Свет" />
            <Tab id={1} title="Музыка" />
            <Tab id={2} title="Стробоскоп" />
          </Tabs>
        </div>

        <Card
          style={{ display: this.state.navbarTabId === 0 ? 'block' : 'none' }}
          className={styles.card}
          elevation={2}
        >
          <div className={styles.title}>Управление светом</div>

          <p>
            <p className={styles.subtitle}>Яркость</p>
            <Slider className={styles.slider}
              labelRenderer={false}
              labelPrecision={0}
              min={0}
              max={100}
              value={this.state.brightness}
              onChange={value => this.changeBrightness(value)}
            />
          </p>

          <p className={styles.pickerContainer}>
            <p ref={ref => ref && this.mountPicker()} id="color-picker" className={styles.colorPicker} />
          </p>
        </Card>

        <Card
          style={{ display: this.state.navbarTabId === 1 ? 'block' : 'none' }}
          className={styles.card}
          elevation={2}
        >
          <div className={styles.title}>Режим светомузыки активирован.</div>

          <p>
            Вы можете подключиться к точке доступа Bluetooth "SmartBulb" и начать воспроизведение.
          </p>
        </Card>

        <Card
          style={{ display: this.state.navbarTabId === 2 ? 'block' : 'none' }}
          className={styles.card}
          elevation={2}
        >
          <div className={styles.title}>Стробоскоп</div>

          <p>
            <div className={styles.subtitle}>Частота (2 — 100 Гц)</div>
            <div>
              <NumericInput
                className={styles.freqInput}
                buttonPosition="none"
                min={2}
                max={100}
                minorStepSize={1}
                stepSize={1}
                onValueChange={freq => this.setStrob(freq)}
                value={this.state.freq}
              />

              Герц
            </div>
          </p>
        </Card>
      </div>
    );
  }
}

export default App;
